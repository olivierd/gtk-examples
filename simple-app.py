#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''
Example how to get dimension of screen without using deprecated
method (e.g., Gdk.Screen.get_width).

For Wayland session, use the GDK_BACKEND environment variable.
'''

import sys

import gi

gi.require_version('GLib', '2.0')
gi.require_version('Gio', '2.0')
gi.require_version('Gtk', '3.0')
gi.require_version('Gdk', '3.0')
gi.require_version('Wnck', '3.0')

from gi.repository import GLib, Gio, Gdk, Gtk

def check_x11_session():
    '''Is it X11 session?'''
    backend_env = GLib.environ_getenv(GLib.get_environ(),
                                      'GDK_BACKEND')
    if backend_env is None:
        # We look for the default display
        default_disp = Gdk.Display.get_default().get_name()
        if default_disp.startswith('wayland'):
            return False
        else:
            return True
    else:
        if backend_env.lower() == 'x11':
            return True
        else:
            return False

if not check_x11_session():
    print('Try GDK_BACKEND=x11 ./{0}'.format(sys.argv[0]))
    sys.exit(-1)
else:
    from gi.repository import Wnck


class SimpleWindow(Gtk.ApplicationWindow):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        # Additional properties of Gtk.ApplicationWindow
        self.props.icon_name = 'application-x-executable'
        self.props.border_width = 3
        self.set_default_size(400, 150)
        self.set_show_menubar(False)

        self.get_screen_size()

    def get_screen_size(self):
        '''Return dimension of screen.'''
        if Gtk.get_minor_version() >= 22:
            # It is a Wnck.Screen object
            screen = Wnck.Screen.get_default()
            if screen is not None:
                print('{0}x{1}'.format(screen.get_width(),
                                       screen.get_height()))
        else:
            # It is a Gdk.Screen object
            screen = Gdk.Screen.get_default()
            if screen is not None:
                print('{0}x{1}'.format(screen.get_width(),
                                       screen.get_height()))

class SimpleApp(Gtk.Application):
    def __init__(self):
        app_id = 'org.framagit.olivierd.simple-app'
        super().__init__(application_id='{0}'.format(app_id),
                         flags=Gio.ApplicationFlags.FLAGS_NONE)

    # Virtual methods
    def do_startup(self):
        Gtk.Application.do_startup(self)

        action = Gio.SimpleAction.new('quit', None)
        action.connect('activate', self.app_quit_cb)
        self.add_action(action)
        self.set_accels_for_action('app.quit', ['<Control>q'])

        GLib.set_application_name('Simple Application')
        GLib.set_prgname('simpleapp')

    def do_activate(self):
        window = SimpleWindow(application=self,
                              title='Simple Application')
        window.show_all()

    # Callback methods
    def app_quit_cb(self, simple_action, param):
        self.quit()


def main(args):
    SimpleApp().run(args)

if __name__ == '__main__':
    main(sys.argv)
