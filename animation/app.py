#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''
Example of Gtk.ColorButton and Gtk.DrawingArea widgets.
'''

import sys

import gi
gi.require_version('GLib', '2.0')
gi.require_version('GObject', '2.0')
gi.require_version('Gio', '2.0')
gi.require_version('Gdk', '3.0')
gi.require_version('Gtk', '3.0')
from gi.repository import GLib, GObject, Gio, Gdk, Gtk
try:
    gi.require_version('Xfconf', '0')
    from gi.repository import Xfconf
except ValueError:
    print('Xfonf GObject Introspection is required')
    sys.exit(0)


class AnimationSettings(GObject.Object):

    # Properties
    prop_width = GObject.Property(type=GObject.TYPE_INT,
                                  default=550,
                                  flags=GObject.ParamFlags.READWRITE)
    prop_height = GObject.Property(type=GObject.TYPE_INT,
                                   default=290,
                                   flags=GObject.ParamFlags.READWRITE)
    prop_subdivision = GObject.Property(type=GObject.TYPE_DOUBLE,
                                        default=5.0,
                                        flags=GObject.ParamFlags.READABLE)

    def __init__(self, channel):
        GObject.Object.__init__(self)

        self.channel = channel

    def get_color_rgb(self):
        '''Return values from the Xfconf properties (/color).'''
        res = []
        default_value = 0.0

        props = self.channel.get_properties('/color')
        if props is not None:
            for key in iter(props):
                if 'red' in key:
                    res.append(self.channel.get_double('/color/red',
                                                       default_value))
                elif 'green' in key:
                    res.append(self.channel.get_double('/color/green',
                                                       default_value))
                elif 'blue' in key:
                    res.append(self.channel.get_double('/color/blue',
                                                       default_value))

        if res:
            return res
        else:
            # Black color
            return 3 * [default_value]

    def set_color_rgb(self, gdk_rgba):
        self.channel.set_double('/color/red', gdk_rgba.red)
        self.channel.set_double('/color/green', gdk_rgba.green)
        self.channel.set_double('/color/blue', gdk_rgba.blue )

    def get_gdk_rgba(self):
        red_value, green_value, blue_value = self.get_color_rgb()

        return Gdk.RGBA(red=red_value, green=green_value,
                        blue=blue_value, alpha=1.0)

    def get_subdivision(self):
        default_value = self.get_property('prop-subdivision')
        res = default_value

        if self.channel.has_property('/subdivision'):
            res = self.channel.get_double('/subdivision', default_value)

        return res

    def set_subdivision(self, value):
        self.channel.set_double('/subdivision', value)

    def get_threshold(self):
        default_value = 0.7
        res = default_value

        if self.channel.has_property('/threshold'):
            res = self.channel.get_double('/threshold', default_value)

        return res

    def set_threshold(self, value):
        self.channel.set_double('/threshold', value)

    def get_window_width(self):
        '''Return value of the Xfconf property (/window/width).'''
        res = self.get_property('prop-width')

        if self.channel.has_property('/window/width'):
            res = self.channel.get_int('/window/width',
                                       self.get_property('prop-width'))

        return res

    def set_window_width(self, value):
        self.channel.set_int('/window/width', value)

    def get_window_height(self):
        '''Return value of the Xfconf property (/window/height).'''
        res = self.get_property('prop-height')

        if self.channel.has_property('/window/height'):
            res = self.channel.get_int('/window/height',
                                       self.get_property('prop-height'))

        return res

    def set_window_height(self, value):
        self.channel.set_int('/window/height', value)

    def is_window_fullscreen(self):
        '''In fact, it is maximized state.'''
        res = False

        if self.channel.has_property('/window/fullscreen'):
            res = self.channel.get_bool('/window/fullscreen', False)

        return res

    def set_window_fullscreen(self, value):
        self.channel.set_bool('/window/fullscreen', value)


class AnimationWindow(Gtk.ApplicationWindow):
    def __init__(self, app, channel):
        Gtk.ApplicationWindow.__init__(self,
                                       application=app,
                                       show_menubar=False)

        self.props.icon_name = 'application-x-executable'
        self.set_title('Color Application')

        self.settings = AnimationSettings(channel)

        self.main_window()

    def hsv_to_rgb(self, hue, saturation, value):
        '''Based-on Python's colorsys module.'''
        i = int(hue * 6.0)
        f = (hue * 6.0) - i

        p = value * (1.0 - saturation)
        q = value * (1.0 - saturation * f)
        t = value * (1.0 - saturation * (1.0 - f))

        i = i % 6

        if i == 0:
            r = value
            g = t
            b = p
        if i == 1:
            r = q
            g = value
            b = p
        if i == 2:
            r = p
            g = value
            b = t
        if i == 3:
            r = p
            g = q
            b = value
        if i == 4:
            r = t
            g = p
            b = value
        if i == 5:
            r = value
            g = p
            b = q

        return Gdk.RGBA(red=r, green=g, blue=b, alpha=1.0)

    def rgb_to_hsv(self, red, green, blue):
        '''Based-on Python's colorsys module.'''
        cmax = max(red, green, blue)
        cmin = min(red, green, blue)

        v = cmax
        if cmin == cmax:
            return (0.0, 0.0, v)

        s = (cmax - cmin) / cmax
        rc = (cmax - red) / (cmax - cmin)
        gc = (cmax - green) / (cmax - cmin)
        bc = (cmax - blue) / (cmax - cmin)
        if red == cmax:
            h = bc - gc
        elif green == cmax:
            h = 2.0 + rc - bc
        else:
            h = 4.0 + gc - rc

        h = (h / 6.0) % 1.0

        return (h, s, v)

    def make_color_ramp(self, hsv_t, max_hsv_t, closed):
        res = []
        total_ncolors = 128

        if closed:
            ncolors = round((total_ncolors / 2) + 1)
        else:
            ncolors = total_ncolors

        # Compute deltas
        dh = (max_hsv_t[0] - hsv_t[0]) / ncolors
        ds = (max_hsv_t[1] - hsv_t[1]) / ncolors
        dv = (max_hsv_t[2] - hsv_t[2]) / ncolors

        for i in list(range(ncolors)):
            res.append(self.hsv_to_rgb((hsv_t[0] + (i * dh)),
                                       (hsv_t[1] + (i * ds)),
                                       (hsv_t[2] + (i * dv))))
        return res

    def randomize_gdk_rgba(self):
        #
        threshold = self.settings.get_threshold()
        # Foreground color
        red_value, green_value, blue_value = self.settings.get_color_rgb()

        if red_value == 0.0 and green_value == 0.0 and blue_value == 0.0:
            color1 = self.rgb_to_hsv(1.0, 1.0, 1.0)
            color2 = self.rgb_to_hsv(red_value, green_value, blue_value)
        elif red_value == 1.0 and green_value == 1.0 and blue_value == 1.0:
            color1 = self.rgb_to_hsv(red_value, green_value, blue_value)
            color2 = self.rgb_to_hsv(0.0, 0.0, 0.0)
        else:
            color1 = self.rgb_to_hsv(red_value, green_value, blue_value)
            color2 = self.rgb_to_hsv((red_value * threshold),
                                     (green_value * threshold),
                                     (blue_value * threshold))

        # Get list of Gdk.RGBA
        ncolors = self.make_color_ramp(color1, color2, True)

        random = GLib.random_int_range(0, len(ncolors))
        return ncolors[random]

    def draw_frame(self, context, widget):
        subdivision = int(self.settings.get_subdivision())

        width = widget.get_allocated_width()
        height = widget.get_allocated_height()

        sw = width / subdivision
        sh = height / subdivision

        for y in list(range(subdivision)):
            for x in list(range(subdivision)):
                # Coordinates
                coord_x = int(x * sw)
                coord_y = int(y * sh)

                rect_width = int(sw - 1) 
                rect_height = int(sh -1)

                Gdk.cairo_set_source_rgba(context,
                                          self.randomize_gdk_rgba())
                context.rectangle(coord_x, coord_y,
                                  rect_width, rect_height)
                context.fill()

    def win_range_value_changed_cb(self, range_scale, user_data):
        widget = user_data
        self.settings.set_threshold(range_scale.get_value())

        widget.queue_draw()

    def win_color_button_clicked_cb(self, button):
        self.settings.set_color_rgb(button.get_rgba())

    def win_configure_event_cb(self, widget, event):
        w = event.width
        h = event.height

        if w >= self.settings.get_window_width():
            self.settings.set_window_width(w)

        if h >= self.settings.get_window_height():
            self.settings.set_window_height(h)

    def win_state_event_cb(self, widget, event):
        if event.changed_mask == Gdk.WindowState.MAXIMIZED:
            if self.settings.is_window_fullscreen():
                # Default size of window
                width = self.settings.get_property('prop-width')
                height = self.settings.get_property('prop-height')

                self.settings.set_window_fullscreen(False)
                self.unmaximize()

                # Resize and update properties
                self.resize(width, height)
                self.set_window_size(width, height)
            else:
                self.settings.set_window_fullscreen(True)
                self.maximize()

    def win_draw_cb(self, widget, cr):
        # Background
        cr.set_source_rgb(0, 0, 0)
        cr.paint()

        # Foreground
        self.draw_frame(cr, widget)

        return False

    def win_value_changed_spinbutton_cb(self, button, user_data):
        widget = user_data

        self.settings.set_subdivision(button.get_value())

        widget.queue_draw()

    def win_draw_iter_cb(self, user_data):
        widget = user_data

        widget.queue_draw()
        return True

    def set_window_size(self, width, height):
        '''Store size of main window.'''
        self.settings.set_window_width(width)
        self.settings.set_window_height(height)

    def win_color_button(self):
        '''Build Gtk.ColorButton.'''
        rgba = self.settings.get_gdk_rgba()
        button = Gtk.ColorButton.new_with_rgba(rgba)

        return button

    def win_spin_button(self):
        '''Build Gtk.SpinButton.'''
        min_value = self.settings.get_property('prop-subdivision')

        button = Gtk.SpinButton.new_with_range(min_value,
                                               5 * min_value,
                                               1.0)
        button.set_update_policy(Gtk.SpinButtonUpdatePolicy.IF_VALID)
        button.set_value(self.settings.get_subdivision())

        return button

    def win_enable_scroll(self, h_scroll=False):
        '''Build Gtk.ScrolledWindow widget.'''
        scrolled_window = Gtk.ScrolledWindow.new(None, None)
        if h_scroll:
            policy = Gtk.PolicyType.AUTOMATIC
        else:
            policy = Gtk.PolicyType.NEVER

        scrolled_window.set_policy(policy,
                                   Gtk.PolicyType.AUTOMATIC)

        return scrolled_window

    def win_drawing_area(self):
        color_area = Gtk.DrawingArea.new()
        color_area.set_size_request(300, 150)
        color_area.connect('draw', self.win_draw_cb)

        return color_area

    def win_scale_widget(self):
        scale = Gtk.Scale.new_with_range(Gtk.Orientation.HORIZONTAL,
                                         0.2, 0.9, 0.1)
        scale.set_value(self.settings.get_threshold())

        return scale

    def win_populate(self):
        view_port = Gtk.Viewport.new(None, None)
        view_port.set_shadow_type(Gtk.ShadowType.NONE)

        grid = Gtk.Grid.new()
        grid.set_column_spacing(10)
        grid.set_row_spacing(10)
        grid.props.margin = 4

        # Children widgets
        label_button = Gtk.Label.new('Select a color:')
        grid.attach(label_button, 0, 0, 1, 1)

        color_button = self.win_color_button()
        grid.attach_next_to(color_button, label_button,
                            Gtk.PositionType.RIGHT, 1, 1)

        label_spin = Gtk.Label.new('Number of subdivision:')
        grid.attach(label_spin, 2, 0, 1, 1)

        spin_button = self.win_spin_button()
        grid.attach_next_to(spin_button, label_spin,
                            Gtk.PositionType.RIGHT, 1, 1)

        label_scale = Gtk.Label.new('Threshold:')
        grid.attach(label_scale, 1, 1, 1, 1)
        
        scale = self.win_scale_widget()
        grid.attach_next_to(scale, label_scale,
                            Gtk.PositionType.RIGHT, 1, 1)

        frame = Gtk.Frame.new('Preview')
        frame.set_shadow_type(Gtk.ShadowType.NONE)
        frame.props.margin = 6

        color_area = self.win_drawing_area()
        frame.add(color_area)
        grid.attach(frame, 1, 2, 2, 1)

        view_port.add (grid)

        GLib.timeout_add(35,
                         self.win_draw_iter_cb,
                         color_area)

        # Signals
        color_button.connect('color_set',
                             self.win_color_button_clicked_cb)
        spin_button.connect('value-changed',
                            self.win_value_changed_spinbutton_cb,
                            color_area)
        scale.connect('value_changed',
                      self.win_range_value_changed_cb,
                      color_area)

        return view_port

    def main_window(self):
        if self.settings.is_window_fullscreen():
            self.maximize()
        else:
            self.props.default_width = self.settings.get_window_width()
            self.props.default_height = self.settings.get_window_height()

        scroll = self.win_enable_scroll(True)
        scroll.add(self.win_populate())
        self.add(scroll)

        # Signals
        self.connect('configure_event', self.win_configure_event_cb)
        self.connect('window_state_event', self.win_state_event_cb)


class AnimationApp(Gtk.Application):
    def __init__(self, channel):
        name_id = 'org.framagit.olivier.gtk-examples'

        Gtk.Application.__init__(self,
                                 application_id='{0}'.format(name_id),
                                 flags=Gio.ApplicationFlags.FLAGS_NONE)

        self.channel = channel

    # Virtual methods
    def do_startup(self):
        Gtk.Application.do_startup(self)

        # Ctrl-q shortcut
        action = Gio.SimpleAction.new('quit', None)
        action.connect('activate', self.app_quit_cb)
        self.add_action(action)
        self.set_accels_for_action('app.quit', ['<Control>q'])

        GLib.set_application_name('Color Application')
        GLib.set_prgname('color_app')

    def do_activate(self):
        window = AnimationWindow(self, self.channel)
        window.show_all()

    # Callback method
    def app_quit_cb(self, action, param):
        #Xfconf.shutdown()

        self.quit()


def main(args):
    channel = None

    if Xfconf.init():
        channel = Xfconf.Channel.get('test-animation')
        AnimationApp(channel).run(args)
    else:
        print('Unable to connect to Xfconf daemon')
        sys.exit(0)


if __name__ == '__main__':
    main(sys.argv)
