#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''
Store dimension of widget, when we resize it.
We support full-screen too.
'''

import sys

import gi
gi.require_version('GLib', '2.0')
gi.require_version('GObject', '2.0')
gi.require_version('Gio', '2.0')
gi.require_version('Gdk', '3.0')
gi.require_version('Gtk', '3.0')
from gi.repository import GLib, GObject, Gio, Gdk, Gtk
try:
    gi.require_version('Xfconf', '0')
    from gi.repository import Xfconf
except ValueError:
    print('Xfonf GObject Introspection is required')
    sys.exit(0)


class AppSettings(GObject.Object):
    # Propreties
    prop_width = GObject.Property(type=GObject.TYPE_INT,
                                  default=350,
                                  flags=GObject.ParamFlags.READWRITE)
    prop_height = GObject.Property(type=GObject.TYPE_INT,
                                   default=450,
                                   flags=GObject.ParamFlags.READWRITE)
    prop_fullscreen = GObject.Property(type=GObject.TYPE_BOOLEAN,
                                       default=False,
                                       flags=GObject.ParamFlags.READWRITE)

    def __init__(self, channel):
        GObject.Object.__init__(self)

        # Easily convert a GObject property name to a Xfconf property
        # name (and vice versa)
        self.props_dict = {'prop-width': '/window/width',
                           'prop-height': '/window/height',
                           'prop-fullscreen': '/window/fullscreen'}

        self._settings_init(channel)

    def _initialize_gobject_value(self, pspec, val):
        g_value = None

        if pspec.value_type == GObject.TYPE_INT:
            g_value = GObject.Value(GObject.TYPE_INT)
            g_value.set_int(val)
        elif pspec.value_type == GObject.TYPE_BOOLEAN:
            g_value = GObject.Value(GObject.TYPE_BOOLEAN)
            g_value.set_boolean(val)

        return g_value

    def _settings_init(self, channel):
        '''Initialize Xfconf properties of given channel.'''

        for pspec in self.list_properties():
            # Get Xfconf property name
            xfconf_prop = self.props_dict.get(pspec.name)

            if not channel.has_property(xfconf_prop):
                g_value = self._initialize_gobject_value(pspec,
                                                         pspec.default_value)
                if g_value is not None:
                    channel.set_property(xfconf_prop, g_value)

    def lookup_property_name(self, prop, reverse=False):
        if reverse:
            d = dict(map(reversed, self.props_dict.items()))
        else:
            d = self.props_dict

        return d.get(prop)

    def set_value(self, channel, prop, value):
        prop_name = self.lookup_property_name(prop, True)
        pspec = self.find_property(prop_name)

        g_value = self._initialize_gobject_value(pspec, value)
        if g_value is not None:
            channel.set_property(prop, g_value)

    def get_int(self, channel, prop):
        g_value = GObject.Value(GObject.TYPE_INT)
        channel.get_property(prop, g_value)
        return g_value.get_int()

    def get_boolean(self, channel, prop):
        g_value = GObject.Value(GObject.TYPE_BOOLEAN)
        channel.get_property(prop, g_value)
        return g_value.get_boolean()


class ResizeWindow(Gtk.ApplicationWindow):
    def __init__(self, app, channel):
        Gtk.ApplicationWindow.__init__(self,
                                       application=app,
                                       show_menubar=False)
        # Additional properties
        self.settings = AppSettings(channel)

        self.props.icon_name = 'application-x-executable'
        self.set_title('Application')

        # Additional properties
        self.settings = AppSettings(channel)

        self.main_window(channel)

    def win_is_fullscreen(self, channel):
        '''In fact, it is maximise state.'''
        res = False

        if channel.has_property('/window/fullscreen'):
            res = self.settings.get_boolean(channel,
                                            '/window/fullscreen')
        return res

    def win_get_width(self, channel):
        if channel.has_property('/window/width'):
            return self.settings.get_int(channel, '/window/width')
        else:
            return self.settings.get_property('prop-width')

    def win_get_height(self, channel):
        if channel.has_property('/window/height'):
            return self.settings.get_int(channel, '/window/height')
        else:
            return self.settings.get_property('prop-height')

    def main_window(self, channel):
        if self.win_is_fullscreen(channel):
            self.maximize()
        else:
            self.props.default_width = self.win_get_width(channel)
            self.props.default_height = self.win_get_height(channel)

        # Signals
        self.connect('configure_event', self.win_size_allocate_cb,
                     channel)
        self.connect('window_state_event', self.win_state_event_cb,
                     channel)

    def win_size_allocate_cb(self, widget, allocation, user_data):
        '''Store new values.'''
        channel = user_data

        w = allocation.width
        h = allocation.height

        if w >= self.win_get_width(channel):
            self.settings.set_value(channel, '/window/width', w)

        if h >= self.win_get_height(channel):
            self.settings.set_value(channel, '/window/height', h)

    def win_state_event_cb(self, widget, event, user_data):
        channel = user_data

        if event.changed_mask == Gdk.WindowState.MAXIMIZED:
            if self.win_is_fullscreen(channel):
                self.settings.set_value(channel, '/window/fullscreen',
                                        False)
                self.unmaximize()

                # Resize and update properties
                width = self.settings.get_property('prop-width')
                height = self.settings.get_property('prop-height')

                self.resize(width, height)
                self.settings.set_value(channel, '/window/width', width)
                self.settings.set_value(channel, '/window/height', height)
            else:
                self.settings.set_value(channel, '/window/fullscreen',
                                        True)
                self.maximize()


class ResizeApp(Gtk.Application):
    def __init__(self, channel):
        Gtk.Application.__init__(self,
                                 application_id='org.framagit.olivierd.resize',
                                 flags=Gio.ApplicationFlags.FLAGS_NONE)

        self.channel = channel

    # Virtual methods
    def do_startup(self):
        Gtk.Application.do_startup(self)

        # Ctrl-q shortcut
        action = Gio.SimpleAction.new('quit', None)
        action.connect('activate', self.app_quit_cb)
        self.add_action(action)
        self.set_accels_for_action('app.quit', ['<Control>q'])

        GLib.set_application_name('Resize application')
        GLib.set_prgname('resize_app')

    def do_activate(self):
        window = ResizeWindow(self, self.channel)
        window.show_all()

    # Callback method
    def app_quit_cb(self, action, param):
        #Xfconf.shutdown()

        self.quit()


def main(args):
    channel = None

    if Xfconf.init():
        channel = Xfconf.Channel.get('resize-app')
        ResizeApp(channel).run(args)
    else:
        print('Unable to connect to Xfconf')
        sys.exit(0)


if __name__ == '__main__':
    main(sys.argv)
