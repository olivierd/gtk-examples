#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''Example how to get the active window without using deprecated
method (e.g., Gdk.Screen.get_active_window).

For Wayland session, use the GDK_BACKEND environment variable.
'''

import sys

import Xlib
import Xlib.display
import gi

gi.require_version('GLib', '2.0')
gi.require_version('Gio', '2.0')
gi.require_version('Gdk', '3.0')
gi.require_version('GdkX11', '3.0')
gi.require_version('Gtk', '3.0')

from gi.repository import GLib, Gio, Gdk, Gtk

def check_x11_session():
    '''Is it X11 session?'''
    backend_env = GLib.environ_getenv(GLib.get_environ(),
                                      'GDK_BACKEND')
    if backend_env is None:
        # We look for the default display
        default_disp = Gdk.Display.get_default().get_name()
        if default_disp.startswith('wayland'):
            return False
        else:
            return True
    else:
        if backend_env.lower() == 'x11':
            return True
        else:
            return False

if not check_x11_session():
    print('Try GDK_BACKEND=x11 ./{0}'.format(sys.argv[0]))
    sys.exit(-1)
else:
    from gi.repository import GdkX11


class Window(Gtk.ApplicationWindow):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        # Additional Gtk.ApplicationWindow properties
        self.props.icon_name = 'application-x-executable'
        self.props.border_width = 3
        self.set_default_size(300, 150)
        self.set_show_menubar(False)

        self.get_display_size()

    def get_active_window(self):
        xdisplay = Xlib.display.Display()
        root_win = xdisplay.screen().root

        active_win = xdisplay.intern_atom('_NET_ACTIVE_WINDOW')
        if active_win is not None:
            result = root_win.get_full_property(active_win,
                                                Xlib.Xatom.WINDOW)
            if result.value[0] > 0:
                return result.value[0]
            else:
                return None
        else:
            return None

    def get_display_size(self):
        display = Gdk.Display.get_default()
        win_id = self.get_active_window()
        if win_id is not None:
            gdk_window = GdkX11.X11Window.foreign_new_for_display(display,
                                                                  win_id)
        else:
            # We fallback to the whole screen
            gdk_window = Gdk.get_default_root_window()

        monitor = display.get_monitor_at_window(gdk_window)
        rect = monitor.get_geometry()
        print(rect.width)

class App(Gtk.Application):
    def __init__(self):
        app_id = 'org.framagit.olivierd.active-win'
        super().__init__(application_id='{0}'.format(app_id),
                         flags=Gio.ApplicationFlags.FLAGS_NONE)

    # Virtual methods
    def do_startup(self):
        Gtk.Application.do_startup(self)

        action = Gio.SimpleAction.new('quit', None)
        action.connect('activate', self.app_quit_cb)
        self.add_action(action)
        self.set_accels_for_action('app.quit', ['<Control>q'])

        GLib.set_application_name('Application')
        GLib.set_prgname('app')

    def do_activate(self):
        window = Window(application=self,
                        title='Application')
        window.show_all()

    # Callback methods
    def app_quit_cb(self, action, param):
        self.quit()


def main(args):
    App().run(args)

if __name__ == '__main__':
    main(sys.argv)
