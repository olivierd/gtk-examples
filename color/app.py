#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''
Example of Gtk.Scale, Gtk.ColorButton and Gtk.DrawingArea widgets.
'''

import sys

import gi
gi.require_version('GLib', '2.0')
gi.require_version('Gio', '2.0')
gi.require_version('Gdk', '3.0')
gi.require_version('Gtk', '3.0')
from gi.repository import GLib, Gio, Gdk, Gtk
try:
    gi.require_version('Xfconf', '0')
    from gi.repository import Xfconf
except ValueError:
    print('Xfonf GObject Introspection is required')
    sys.exit(0)


class ColorWindow(Gtk.ApplicationWindow):
    def __init__(self, app, channel):
        Gtk.ApplicationWindow.__init__(self,
                                       application=app,
                                       show_menubar=False)

        self.props.icon_name = 'application-x-executable'
        self.set_title('Color Application')

        self.channel = channel
        self.width = 650
        self.height = 300
        self.scale_label = Gtk.Label.new(None)
        self.msg = 'Current value of alpha channel: {0}'

        self.main_window(channel)

    def win_range_value_changed_cb(self, obj):
        '''Store value of Gtk.Scale widget.'''

        self.channel.set_double('/color/alpha', obj.get_value())

    def win_color_button_clicked_cb(self, button):
        # Get Gdk.RGBA object
        rgb = button.get_rgba()
        #print('{0:f} {1:f} {2:f}'.format(rgb.red, rgb.green, rgb.blue))

        self.channel.set_double('/color/red', rgb.red)
        self.channel.set_double('/color/green', rgb.green)
        self.channel.set_double('/color/blue', rgb.blue)

    def win_configure_event_cb(self, widget, event):
        w = event.width
        h = event.height

        if w >= self.get_window_width():
            self.channel.set_int('/window/width', w)

        if h >= self.get_window_height():
            self.channel.set_int('/window/height', h)

    def win_state_event_cb(self, widget, event):
        if event.changed_mask == Gdk.WindowState.MAXIMIZED:
            if self.is_fullscreen():
                self.channel.set_bool('/window/fullscreen', False)
                self.unmaximize()

                # Resize and update properties
                self.resize(self.width, self.height)
                self.set_window_size(self.width, self.height)
            else:
                self.channel.set_bool('/window/fullscreen', True)
                self.maximize()

    def xfconf_property_changed_cb(self, channel, prop, val):
        
        if prop == '/color/alpha':
            self.scale_label.set_text(self.msg.format(val))
        else:
            self.scale_label.set_text(self.msg.format(self.get_color_alpha()))

    def win_draw_cb(self, widget, cr):
        red_value, green_value, blue_value = self.get_color_rgb()

        cr.set_source_rgb(red_value, green_value, blue_value)
        cr.paint()

        return False

    def is_fullscreen(self):
        '''In fact, it is maximized state.'''
        res = False

        if self.channel.has_property('/window/fullscreen'):
            res = self.channel.get_bool('/window/fullscreen', False)

        return res

    def get_color_alpha(self):
        '''Return value of the Xfconf property (/color/alpha).'''
        # Opaque
        res = 1.0

        if self.channel.has_property('/color/alpha'):
            res = self.channel.get_double('/color/alpha', 1.0)

        return res

    def get_color_rgb(self):
        '''Return values from the Xfconf properties (/color).'''
        res = []
        default_value = 0.0

        props = self.channel.get_properties('/color')
        if props is not None:
            for key in iter(props):
                if 'red' in key:
                    res.append(self.channel.get_double('/color/red',
                                                       default_value))
                elif 'green' in key:
                    res.append(self.channel.get_double('/color/green',
                                                       default_value))
                elif 'blue' in key:
                    res.append(self.channel.get_double('/color/blue',
                                                       default_value))

        if res:
            return res
        else:
            # Black color
            return 3 * [default_value]

    def get_window_width(self):
        '''Return value of the Xfconf property (/window/width).'''
        res = self.width

        if self.channel.has_property('/window/width'):
            res = self.channel.get_int('/window/width', self.width)

        return res

    def get_window_height(self):
        '''Return value of the Xfconf property (/window/height).'''
        res = self.height

        if self.channel.has_property('/window/height'):
            res = self.channel.get_int('/window/height', self.height)

        return res

    def set_window_size(self, width, height):
        '''Store size of main window.'''
        self.channel.set_int('/window/width', width)
        self.channel.set_int('/window/height', height)

    def win_color_button(self):
        '''Build Gtk.ColorButton.'''
        red_value, green_value, blue_value = self.get_color_rgb()

        rgba = Gdk.RGBA(red=red_value, green=green_value,
                        blue=blue_value, alpha=1.0)
        button = Gtk.ColorButton.new_with_rgba(rgba)

        return button

    def win_enable_scroll(self, h_scroll=False):
        '''Build Gtk.ScrolledWindow widget.'''
        scrolled_window = Gtk.ScrolledWindow.new(None, None)
        if h_scroll:
            policy = Gtk.PolicyType.AUTOMATIC
        else:
            policy = Gtk.PolicyType.NEVER

        scrolled_window.set_policy(policy,
                                   Gtk.PolicyType.AUTOMATIC)

        return scrolled_window

    def win_left_side(self):
        view_port = Gtk.Viewport.new(None, None)
        view_port.set_shadow_type(Gtk.ShadowType.NONE)

        vbox = Gtk.Box.new(Gtk.Orientation.VERTICAL, 2)
        top = Gtk.Box.new(Gtk.Orientation.HORIZONTAL, 0)
        bottom = Gtk.Box.new(Gtk.Orientation.HORIZONTAL, 0)

        scale = Gtk.Scale.new_with_range(Gtk.Orientation.HORIZONTAL,
                                         0.0, 1.0, 0.5)
        scale.set_value(self.get_color_alpha())
        scale.set_draw_value(False)
        scale.set_round_digits(2)

        color_button = self.win_color_button()

        top.pack_start(Gtk.Label.new('Alpha channel: '),
                                     True, True, 0)
        top.pack_end(scale, True, True, 0)
        vbox.pack_start(top, True, True, 0)

        bottom.pack_start(Gtk.Label.new('Select a color: '),
                          True, True, 0)
        bottom.pack_end(color_button, True, False, 0)
        vbox.pack_end(bottom, True, False, 0)

        # Signals
        scale.connect('value_changed', self.win_range_value_changed_cb)
        color_button.connect('color_set', self.win_color_button_clicked_cb)

        view_port.add(vbox)
        return view_port

    def win_right_side(self):
        view_port = Gtk.Viewport.new(None, None)
        view_port.set_shadow_type(Gtk.ShadowType.NONE)

        vbox = Gtk.Box.new(Gtk.Orientation.VERTICAL, 2)

        self.scale_label.set_text(self.msg.format(self.get_color_alpha()))

        color_area = Gtk.DrawingArea.new()
        color_area.set_size_request(150, 150)
        color_area.connect('draw', self.win_draw_cb)

        vbox.pack_start(self.scale_label, True, True, 0)
        vbox.pack_end(color_area, False, False, 0)

        view_port.add(vbox)
        return view_port

    def win_splitted_window(self):
        paned = Gtk.Paned.new(Gtk.Orientation.HORIZONTAL)
        paned.props.position = 280
        paned.props.position_set = True

        # Scrolled widgets
        scrolled_left = self.win_enable_scroll(True)
        scrolled_right = self.win_enable_scroll(True)

        # Left side
        scrolled_left.add(self.win_left_side())
        paned.add1(scrolled_left)
        # Right side
        scrolled_right.add(self.win_right_side())
        paned.add2(scrolled_right)

        return paned

    def main_window(self, channel):
        if self.is_fullscreen():
            self.maximize()
        else:
            self.props.default_width = self.get_window_width()
            self.props.default_height = self.get_window_height()

        self.add(self.win_splitted_window())

        # Signals
        self.connect('configure_event', self.win_configure_event_cb)
        self.connect('window_state_event', self.win_state_event_cb)
        self.channel.connect('property_changed',
                             self.xfconf_property_changed_cb)


class ColorApp(Gtk.Application):
    def __init__(self, channel):
        name_id = 'org.framagit.olivier.gtk-examples'

        Gtk.Application.__init__(self,
                                 application_id='{0}'.format(name_id),
                                 flags=Gio.ApplicationFlags.FLAGS_NONE)

        self.channel = channel

    # Virtual methods
    def do_startup(self):
        Gtk.Application.do_startup(self)

        # Ctrl-q shortcut
        action = Gio.SimpleAction.new('quit', None)
        action.connect('activate', self.app_quit_cb)
        self.add_action(action)
        self.set_accels_for_action('app.quit', ['<Control>q'])

        GLib.set_application_name('Color Application')
        GLib.set_prgname('color_app')

    def do_activate(self):
        window = ColorWindow(self, self.channel)
        window.show_all()

    # Callback method
    def app_quit_cb(self, action, param):
        #Xfconf.shutdown()

        self.quit()


def main(args):
    channel = None

    if Xfconf.init():
        channel = Xfconf.Channel.get('test-color')
        ColorApp(channel).run(args)
    else:
        print('Unable to connect to Xfconf daemon')
        sys.exit(0)


if __name__ == '__main__':
    main(sys.argv)
